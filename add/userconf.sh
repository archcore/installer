#!/bin/sh

# nanorc
git clone https://gitlab.com/coreconf/nanorc.git
mv nanorc/.nano ~
mv nanorc/.nanorc ~
rm -rf nanorc

# mc
mkdir -p ~/.config/mc/
git clone https://gitlab.com/coreconf/mc.git ~/.config/mc/
rm -rf ~/.config/mc/.git

# clean
rm userconf.sh

# ohmyzsh
sh -c "$(wget https://gitlab.com/coreconf/ohmyzsh/raw/master/tools/install.sh -O -)"
