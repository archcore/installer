#!/bin/sh

set -ex

# partion
echo -e "o\nn\np\n1\n\n\nw" | fdisk /dev/sda
mkfs.ext4 -L p_archcore /dev/sda1
mount -L p_archcore /mnt
mkdir /mnt/{boot,home}

# pacman
mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bk0
curl -o /etc/pacman.d/mirrorlist 'https://www.archlinux.org/mirrorlist/?country=DE&protocol=https'
sed -i 's/^#Server = https/Server = https/' /etc/pacman.d/mirrorlist
pacman-key --init
pacman-key --populate
pacman -Sy
pacman -S archlinux-keyring --noconfirm

# pacstrap
pacstrap /mnt base base-devel
genfstab -Lp /mnt > /mnt/etc/fstab

# prepare
echo "chmod +x stage2.sh" > /mnt/root/.bashrc
echo "./stage2.sh" >> /mnt/root/.bashrc
cp stage2.sh /mnt/
cp add/userconf.sh /mnt/root
cp add/userconf.sh /mnt/

# change-root
arch-chroot /mnt/
