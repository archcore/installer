## Archcore Linux
### Arch Linux with a bunch of scripts

This script will install the Archcore Linux system

## Integrates

1. additional `packages`:

```
git
haveged
htop
mc
openssh
rsync
screen
wget
zsh 
```

2. modified `/etc/ssh/sshd_config`:

```
Protocol 2
Port ${SSH_PORT}
LoginGraceTime 30
PermitRootLogin no
AuthorizedKeysFile .ssh/authorized_keys
PubkeyAuthentication yes
PermitEmptyPasswords no
PasswordAuthentication no
ServerKeyBits 4096
KeyRegenerationInterval 3600
SyslogFacility AUTH
LogLevel INFO
AllowUsers ${USER_1}
AddressFamily inet
MaxAuthTries 3
StrictModes yes
IgnoreRhosts yes
UseDNS no
HostbasedAuthentication no
ChallengeResponseAuthentication yes
AuthenticationMethods publickey,keyboard-interactive
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes128-ctr
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com
HostKeyAlgorithms ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-rsa
KexAlgorithms curve25519-sha256@libssh.org
X11Forwarding no
PrintMotd no
PrintLastLog yes
TCPKeepAlive no
UsePrivilegeSeparation yes
UsePAM yes
```

3. enabled `services`:

```
dhcpcd.service 
haveged.service 
sshd.service
```

## coreconf included

1. modified `nanorc`-files:

see: https://github.com/scopatz/nanorc 

2. oh-my-zsh with some alias and theme `xiong-chiamiov-plus`

> alias cp="rsync -avzP" \
> alias p="pacman" \
> alias mc="EDITOR=nano mc" \
> alias nano="nano -c"

3. modified midnight-commander `ini`:

> use_internal_edit=false \
> skin=modarcon16-defbg